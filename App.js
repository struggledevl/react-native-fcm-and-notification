/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component, useEffect } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
  AsyncStorage,
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';

import firebase from '@react-native-firebase/app';
import messaging from "@react-native-firebase/messaging";

class App extends Component {
  // constructor(props) {
  //   super(props);
  //   this.state = {
  //   }
  // }

  // Retrieve the current registration token
  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      // user doesn't have a device token yet
      fcmToken = await firebase.messaging().getAPNSToken();
      if (fcmToken) {
        // user has a device token
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }
  }

  // Check permissions
  async checkPermission() {
    const enabled = await firebase.messaging().requestPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  // Requesting permissions
  // async requestPermission() {
  //   const granted = firebase.messaging().requestPermission();
  //   if (granted) {
  //     console.log('User granted messaging permissions!');
  //     this.getToken();
  //   } else {
  //     console.log('User declined messaging permissions :');
  //     this.requestPermission();
  //   }
  // }

  // Request permissions
  async requestPermission() {
    try {
      await firebase.messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }

  // testing set Background messages function from 
  // https://invertase.io/oss/react-native-firebase/v6/messaging/quick-start
  async backgroundMessage() {
    messaging().setBackgroundMessageHandler(async remoteMessage => {
      console.log('Message handled in the background!', remoteMessage);
    });
  }
  

  async createNotificationListener() {
    // Build notification as above
    // firebase.notifications().onNotification(notification => {
    //   notification.android.setChannelId('insider').setSound('default')
    //   firebase.notifications().displayNotification(notification) // <-- Display the notification
    //   // this.setState({
    //   //   notif: notification
    //   // })
    // });
  }

  componentDidMount() {
    // const channel = new firebase.notifications.Android.Channel('insider', 'insider channel', firebase.notifications.Android.Importance.Max)
    // firebase.notifications().android.createChannel(channel);
    this.checkPermission();
    this.createNotificationListener();
    this.backgroundMessage();  // <-- set Background messages function to componentDidMount
  }

  render() {
    // const { notif } = this.state
    // console.log('notification: ' + notif)
    return (
      <View style={styles.container}>
        <Text style={styles.title}>
          React Native Firebase Cloud Messagging & Push Notification
        </Text>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#5CB456'
  },
  title: {
    margin: 35,
    textAlign: 'center',
    fontWeight: 'bold',
    fontSize: 16,
    color: 'white'
  }
});

export default App;
