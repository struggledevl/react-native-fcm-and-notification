/**
 * @format
 */
import messaging from '@react-native-firebase/messaging';
import { AppRegistry } from 'react-native';
import App from './App';
import { name as appName } from './app.json';

// messaging().setBackgroundMessageHandler(async remoteMessage => {
//     console.log('Message handled in the background!', remoteMessage);
// });

// function App() {
//     useEffect(() => {
//         const unsubscribe = messaging().onMessage(async remoteMessage => {
//             console.log('FCM Message Data:', remoteMessage.data);
//         });

//         return unsubscribe;
//     }, []);

//     // etc...
// }

AppRegistry.registerComponent(appName, () => App);
